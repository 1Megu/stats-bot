# PoolChia.com Stats Bot
[![pipeline status](https://gitlab.com/1Megu/stats-bot/badges/main/pipeline.svg)](https://gitlab.com/1Megu/stats-bot/-/commits/main) [![Crowdin](https://badges.crowdin.net/poolchia-stats-bot/localized.svg)](https://crowdin.com/project/poolchia-stats-bot) [![Donate XCH Address](https://img.shields.io/badge/XCH-xch157n8unf3fvplf408hdxul38j9fhsrlv6yujryswrwgzqr8ld4vlqt4u35r-green)](https://i.imgur.com/HOu1N2O.png)

A bot for reporting user stats for the Chia pool PoolChia to users of the Discord server.

# Installation
Ensure your environment is setup correctly. This project was built against `node v16.8.0` and older versions may not be compatible. Discord.js requires `node>=16.6.0`.
1. Clone the repository and download dependencies
    ```bash
    git clone https://gitlab.com/1Megu/stats-bot
    cd stats-bot
    npm install
    ```
2. Copy the `config.example.json` to `config.json` and open this with your favourite editor. Enter your bot user's Client ID and Token from [Discord Developer Portal](https://discord.com/developers/applications). Add the channel ID you wish to post block notifications to (if required).
3. Run the project with Node or a process manager (I recommend PM2)

    `node index.js`

    or

    `pm2 start index.js`

# Changelog
See [CHANGELOG.md](CHANGELOG.md).

# License
See [LICENSE](LICENSE)
