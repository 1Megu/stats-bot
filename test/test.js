const assert = require('assert');
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('./test.sqlite');
const conv = require('../modules/conversions');
const helpers = require('../modules/helpers');

describe('DB', function() {
  describe('Create table', function() {
    it('should succeed', function(done) {
      db.run("CREATE TABLE testData (id INT, data TEXT)", (err) => {
          if(err) done(err);
          else done();
      });
    });
    it('should add data', function(done) {
        db.run("INSERT INTO testData VALUES (?, ?)", 1, "mocha", (err) => {
            if(err) done(err);
            else done();
        });
      });
  });
  describe('Read from table', function() {
      it('should return row', function(done) {
          db.get("SELECT * FROM testData WHERE id = ?", 1, (err, row) => {
              if(err) done(err);
              else if(row == undefined) done('No results');
              else done();
          });
      });
    it('should return correct data', function(done) {
        db.get("SELECT * FROM testData WHERE id = ?", 1, (err, row) => {
            if(err) done(err);
            else if(row == undefined) done('No results');
            else if(row.data == 'mocha') done();
        });
    });
  });
  describe('Remove table', function() {
    it('should successfully delete table', function(done) {
      db.run("DROP TABLE testData", (err) => {
        if(err) done(err);
        else done();
      });
    });
  });
});
describe('Conversions', function() {
  describe('Data', function() {
    it('135 B = 135 B', () => {
      assert.equal(conv.data(135), '135 B');
    })
    it('1024 B = 1.00 KiB', () => {
      assert.equal(conv.data(1024), '1.00 KiB');
    });
    it('3,670,016 B = 3.50 MiB', () => {
      assert.equal(conv.data(3670016), '3.50 MiB');
    });
    it('1,073,741,824 B = 1.00 GiB', () => {
      assert.equal(conv.data(1073741824), '1.00 GiB');
    });
  });
});
describe('Helpers', function() {
  describe('Time Conversion', function() {
    it('10s = 10,000ms', () => {
      assert.equal(helpers.s(10), 10000);
    });
    it('1m = 60,000ms', () => {
      assert.equal(helpers.m(1), 60000);
    });
  });
  describe('Zero padding', () => {
    it('13 = 13', () => {
      assert.equal(helpers.zero(13), 13);
    });
    it('3 = 03', () => {
      assert.equal(helpers.zero(3), '03');
    });
    it('3 = 003', () => {
      assert.equal(helpers.zero3(3), '003');
    });
    it('13 = 013', () => {
      assert.equal(helpers.zero3(13), '013');
    });
    it('354 = 354', () => {
      assert.equal(helpers.zero3(354), 354);
    });
  });
  describe('Elapsed time', () => {
    let now = Date.now();
    it('Now -30 seconds = 30 seconds ago', () => {
      assert.equal(helpers.elapsed(now - 30000), '30 seconds ago');
    })
    it('Now -5 minutes = 5 minutes ago', () => {
      assert.equal(helpers.elapsed(now - 5*60*1000), '5 minutes ago');
    })
    it('Now -4 hours = 4 hours ago', () => {
      assert.equal(helpers.elapsed(now - 4*60*60*1000), '4 hours ago');
    });
    it('Now -3 days = 3 days ago', () => {
      assert.equal(helpers.elapsed(now - 3*24*60*60*1000), '3 days ago');
    });
    it('Now -2 weeks = 2 weeks ago', () => {
      assert.equal(helpers.elapsed(now - 2*7*24*60*60*1000), '2 weeks ago');
    });
  });
});