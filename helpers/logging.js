module.exports.log = (msg) => {
    const settings = { "locale": "en-GB", "format": { "timeStyle": "medium" } };
    return console.log(`[${new Intl.DateTimeFormat(settings.locale, settings.format).format(new Date())}] ${msg}`);
}