const settings = {
    data: {
        'GiB': 1073741824,
        'TiB': 1099511627776,
        'PiB': 1125899906842624,
        'EiB': 1152921504606846976
    }
}

module.exports.data = (B) => {
    if(B > settings.data.TiB){
        return `${(B / settings.data.TiB).toFixed(2)} TiB`
    }
    if(B > settings.data.PiB){
        return `${(B / settings.data.PiB).toFixed(2)} PiB`
    }
    if(B > settings.data.EiB){
        return `${(B / settings.data.EiB).toFixed(2)} EiB`
    }
    return `${(B / settings.data.GiB).toFixed(2)} GiB`;
}