module.exports = {
    'DISCORD_CLIENTID': '', // Client ID from: https://discord.com/developers/applications
    'DISCORD_TOKEN': '', // Bot login token from: https://discord.com/developers/applications
    'GUILD_ID': '', // Guild to register the commands under
    'NETSPACE_CHANNEL': { // Channel ID for reporting netspace
        'enabled': true,
        'id': ''
    },
    'POOLSIZE_CHANNEL': { // Channel ID for reporting pool netspace
        'enabled': true,
        'id': ''
    },
    'BLOCKS_TODAY_CHANNEL': { // Channel ID for Blocks Won today
        'enabled': true,
        'id': ''
    },
    'BLOCKS_YDAY_CHANNEL':{ // Channel ID for Blocks Won yesterday
        'enabled': true,
        'id': ''
    },
    'POOL_LUCK_CHANNEL':{ // Channel ID for current pool luck
        'enabled': true,
        'id': ''
    },
    'POOL_LUCK_HISTORY_CHANNEL':{
        'enabled': true,
        'id': ''
    },
    'BLOCKS_PDAY_CHANNEL': { // Channel ID for estimated blocks per day
        'enabled': true,
        'id': ''
    },
    'POOL_BLOCK_CHANNEL': { // Channel ID to post pool block win notifications
        'enabled': true,
        'id': ''
    }, 
    'BLOCK_WIN_CHANNEL': { // Channel ID to post user block win notifications
        'enabled': true,
        'id': ''
    },
    'CHIA_PRICE_CHANNEL': { // Channel ID to post current Chia price
        'enabled': true,
        'id': ''
    },
    'INTL_LOCALE': 'en-GB', // Locale for Time and Date for embeds
    'NETSPACE_API': '', // Netspace API endpoint, should return an object with property netspace listing Chia netspace in bytes
    'POOLSIZE_API': '', // Pool's POOL_STAT API
    'BLOCKS_API': '', // Pool's BLOCKS API
    'POOL_WALLET': '' // Pool's WALLET address
}
