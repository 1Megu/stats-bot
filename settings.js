module.exports.logging = {
    "LOG_LEVEL": "debug", // Valid options: debug, warn, error
    "LOCALE": "en-GB", // See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl#locale_identification_and_negotiation
    "FORMAT": { timeStyle: 'medium', timeZone: 'UTC' } // Intl.DateTimeFormat options object
}
