This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# 2.0.7 - 2021-11-16
## Additions
- Added current Chia price

# 2.0.6 - 2021-09-14
## Additions
- Swapped text-post block wins to embeds

# 2.0.5 - 2021-09-14
## Additions
- Added Pool wallet address to block notifications.
- Added French (thanks @JayceDoubleB#0466)

# 2.0.4 - 2021-09-11
## Additions
- German (thanks @XCH-Addict#4940)
- Localisation for relative time formatting

## Fixes
- Fixed a date constructor I didn't localise

# 2.0.3 - 2021-09-11
## Additions
- Added support for changing language

# 2.0.2 - 2021-09-11
## Fixes
- Added correct config setting for pool luck history

# 2.0.1 - 2021-09-11
Quick hotfix release
## Fixes
- Moved guild id and luck history channels to config.js
- Fixed error on no-payments from guild

# 2.0.0 - 2021-09-11
Backend overhaul, requires new database so users are required to re-register.
## Additions
- Added optional wallet stats
- Added ability to change Launcher ID.
- Added support for localisation
- Added Greek (thanks @katotheo#6700)
- Added Pool Block notifications
- Added 24 hour and 7 day luck statistics
- Added "listening to" presence

## Fixes
- Refactored code to allow faster bot responses
- Catch some more edge-case errors
- Improved update rate of stats
- Only update stats if they have changed
- Reduce number of API calls to improve performance

# 1.1.1 - 2021-08-31
## Additions
- Added GiB, TiB, PiB and EiB definitions for estimated pool size
- Now automatically strips the 0x from Launcher ID
- Added $/XCH to Embed (thanks @katotheo#6700)

## Fixes
- Further optimised code

# 1.1.0 - 2021-08-30
## Additions
- Automated updates of Chia Netspace for XCH/day calculation
- Automated updates of Chia Price for $/day calculation
- Last payment API for Embed

## Fixes
- More error catching to improve stability
- Optimised code to speed up response

# 1.0.0 - Initial Release - 30/08/2021
Project moved to v1 with public release. Basic functionality working.
