const moment = require('moment');

// Convert seconds to ms
module.exports.s = (s) => {
    return s*1000;
}
// Convert minutes to ms
module.exports.m = (m) => {
    return m*60000;
}

const relativeTimeFormat = (value, unit, lang) => {
    return new Intl.RelativeTimeFormat(lang, { numeric: 'always', style: 'long' }).format(Math.round(value), unit);
}

module.exports.elapsed = (dt, lang) => {
    let now = Date.now();
    let diff = (now - dt) / 1000;
    if(diff < 60){
        return relativeTimeFormat(-diff, 'second', lang);
    }
    if(diff < 3600){
        return relativeTimeFormat(-diff / 60, 'minute', lang);
    }
    if(diff < 86400){
        return relativeTimeFormat(-diff / 3600, 'hour', lang);
    }
    if(diff < 604800){
        return relativeTimeFormat(-diff / 86400, 'day', lang);
    }
    if(diff > 604800){
        return relativeTimeFormat(-diff / 604800, 'week', lang);
    }
}

module.exports.schedule = (func, ms, args) => {
    func();
    setInterval(func, ms);
}

module.exports.zero = (i) => {
    if(i < 10){
        return '0' + i;
    }
    return i;
}

module.exports.zero3 = (i) => {
    if(i < 10) return '00' + i;
    if(i < 100) return '0' + i;
    return i;
}

module.exports.zeroDate = (day) => {
    if(day === 0) return 1;
}

module.exports.parseDate = (dt) => {
    return moment.unix(dt);
}

module.exports.today = () => {
    let dt = moment();
    return moment(`${dt.format('YYYY-MM-DD')}T00:00:00Z`).utc();
}

module.exports.yesterday = () => {
    let dt = moment();
    return moment(`${dt.format('YYYY-MM-DD')}T00:00:00Z`).utc().subtract(1, 'days');
}

module.exports.last24 = () => {
    return moment().utc().subtract(1, 'days');
}

module.exports.last7 = () => {
    return moment().utc().subtract(7, 'days');
}