module.exports.data = (B) => {
    const vars = {
        "KiB": 1024,
        "MiB": 1048576,
        "GiB": 1073741824,
        "TiB": 1099511627776,
        "PiB": 1125899906842624,
        "EiB": 1152921504606846976,
        "ZiB": 1180591620717411303424
    }
    if(B < vars.KiB){
        return B + ' B';
    }
    if(B < vars.MiB){
        return (B / vars.KiB).toFixed(2) + ' KiB';
    }
    if(B < vars.GiB){
        return (B / vars.MiB).toFixed(2) + ' MiB';
    }
    if(B < vars.TiB){
        return (B / vars.GiB).toFixed(2) + ' GiB';
    }
    if(B < vars.PiB){
        return (B / vars.TiB).toFixed(2) + ' TiB';
    }
    if(B < vars.EiB){
        return (B / vars.PiB).toFixed(2) + ' PiB';
    }
    if(B < vars.ZiB){
        return (B / vars.EiB).toFixed(2) + ' EiB';
    }
    if(B >= vars.ZiB){
        return (B / vars.ZiB).toFixed(2) + ' ZiB';
    }
}