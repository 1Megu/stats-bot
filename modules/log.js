module.exports.log = (msg, logLevel) => {
    let dt = new Intl.DateTimeFormat(settings.LOCALE, settings.FORMAT).format(new Date());
    switch(logLevel){
        case 'debug':
            if(logVerbosity === 0){
                return console.log(colours.blue + `[${dt}][DEBUG] ${msg}` + colours.reset);
            }
            return;
        case 'info':
            if(logVerbosity < 2){
                return console.log(colours.green + `[${dt}][INFO] ${msg}` + colours.reset);
            }
            return;
        case 'warn':
            if(logVerbosity < 3){
                return console.log(colours.orange + `[${dt}][WARN] ${msg}` + colours.reset);
            }
            return;
        case 'error':
            return console.log(colours.red + `[${dt}][ERROR] ${msg}` + colours.reset);
        default:
            return console.log(msg);
    }
}

const settings = require('../settings').logging;

const colours = {
    "red": "",
    "green": "",
    "blue": "",
    "reset": ""
}

let logVerbosity = 2;

switch(settings.LOG_LEVEL){
    case 'debug':
        return logVerbosity = 0;
    case 'info':
        return logVerbosity = 1;
    case 'warn':
        return logVerbosity = 2;
    case 'error':
        return logVerbosity = 3;
    default:
        break;
}