const { MessageEmbed } = require('discord.js');

let gifs = ['https://c.tenor.com/C3HdrmcGJIcAAAAC/surprised-chris-pratt.gif', 'https://c.tenor.com/oxNNSbyH0aQAAAAC/surprise-shock.gif', 'https://c.tenor.com/ueq07mOMkI8AAAAC/pikachu-shocked.gif', 'https://c.tenor.com/teFSAfy_xVQAAAAC/michael-scott-steve-carell.gif'];

module.exports.poolBlock = (client, channel, blocks, timestamp, heights, wallet, commit) => {

    const embed = new MessageEmbed()
        .setColor('0x2ecc71')
        .setAuthor('PoolChia.com Stats Bot', 'https://cdn.discordapp.com/icons/839994877479223296/5c17ebde41d03dabccb250709e391cb8.png?size=128')
        .setTimestamp()
        .setFooter(commit)
    if(blocks === 1){
        embed.setTitle('We got one! :partying_face:')
        embed.setDescription(`${new Intl.DateTimeFormat('en-GB', { timeStyle: 'long', dateStyle: 'medium', timeZone: 'UTC' }).format(timestamp)}\n\nHeight: ${heights[0]}\n\n[Pool Wallet](https://www.chiaexplorer.com/blockchain/address/${wallet})`);
        return client.channels.cache.get(channel).send({embeds: [embed] });
    }
    if(blocks === 2){
        embed.setTitle(':tada: Double Block! :tada:')
        embed.setDescription(`${new Intl.DateTimeFormat('en-GB', { timeStyle: 'long', dateStyle: 'medium', timeZone: 'UTC' }).format(timestamp)}\n\nHeights: ${heights[0]} & ${heights[1]}\n\n[Pool Wallet](https://www.chiaexplorer.com/blockchain/address/${wallet})`);
        return client.channels.cache.get(channel).send({embeds: [embed] });
    }
    if(blocks === 3){
        embed.setTitle('Triple-Block!')
        embed.setThumbnail('https://cdn.discordapp.com/emojis/883172460843925595.gif?v=1')
        embed.setDescription(`${new Intl.DateTimeFormat('en-GB', { timeStyle: 'long', dateStyle: 'medium', timeZone: 'UTC' }).format(timestamp)}\n\nHeights: ${heights[0]}, ${heights[1]} & ${heights[2]}\n\n[Pool Wallet](https://www.chiaexplorer.com/blockchain/address/${wallet})`);
        let i = Math.floor(Math.random() * gifs.length);
        embed.setImage(gifs[i]);
        return client.channels.cache.get(channel).send({embeds: [embed] });
    }
    if(blocks > 3){
        let height = `${heights[0]}`;
        for(let i=1;i<blocks-1;i++){
            height = height + `, ${heights[i]}`;
        }
        height = height + ` & ${heights[blocks-1]}`
        embed.setTitle('Multi-Block!')
        embed.setThumbnail('https://cdn.discordapp.com/emojis/883172460843925595.gif?v=1')
        embed.setDescription(`${new Intl.DateTimeFormat('en-GB', { timeStyle: 'long', dateStyle: 'medium', timeZone: 'UTC' }).format(timestamp)}\n\nHeights: ${height}\n\n[Pool Wallet](https://www.chiaexplorer.com/blockchain/address/${wallet})`);
        let i = Math.floor(Math.random() * gifs.length);
        embed.setImage(gifs[i]);
        return client.channels.cache.get(channel).send({embeds: [embed] });
    }
}
