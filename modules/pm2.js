const io = require('@pm2/io');

const pm2Requests = io.counter({
    name: 'Total Requests',
    id: 'app/requests'
});
const pm2Users = io.metric({
    name: 'Number of registered users',
    id: 'app/users'
});

const pm2StatsRequests = io.counter({
    name: 'Total stats requests',
    id: 'app/requests/stats'
});

const pm2RegisterRequests = io.counter({
    name: 'Total registration requests',
    id: 'app/requests/register'
});

module.exports.updateUsers = (db) => {
    db.all("SELECT id FROM users", (err, rows) => {
        if(err) return `Error updating users: ${err}`;
        pm2Users.set(rows.length);
    });
    return 'Updated app/users';
}

module.exports.stats = () => {
    pm2Requests.inc();
    pm2StatsRequests.inc();
    return 'Updated app/requests/stats';
}

module.exports.register = () => {
    pm2Requests.inc();
    pm2RegisterRequests.inc();
    return 'Updated app/requests/register';
}