const { REST } = require('@discordjs/rest');
const axios = require('axios');
const { Routes } = require('discord-api-types/v9');
const { Client, Intents } = require('discord.js');
const { SlashCommandBuilder } = require('@discordjs/builders');
const client = new Client({ intents: [Intents.FLAGS.GUILDS] });
const sqlite3 = require('sqlite3');
const db = new sqlite3.Database('./data.sqlite');
const config = require('./config');
const pm2 = require('./modules/pm2');
const { log } = require('./modules/log');
const { s, m, elapsed, schedule, zero, zero3, zeroDate, parseDate, today, yesterday, last24, last7 } = require('./modules/helpers');
const convert = require('./modules/conversions');
const channelUpdate = require('./modules/channelUpdate');
const { poolBlock } = require('./modules/channelPost');
const fs = require('fs');
const { CHIA_PRICE_CHANNEL } = require('./config.example');

let l10n = {};

let langs = fs.readdirSync('./l10n');
for(let i=0;i<langs.length;i++){
    let name = langs[i].split('.');
    l10n[name[0]] = JSON.parse(fs.readFileSync(`./l10n/${langs[i]}`));
}

let build;

if(process.env.NODE_ENV === 'production'){
    build = fs.readFileSync('./.git/refs/heads/main').toString().substr(0, 8);
} else {
    build = fs.readFileSync('./.git/refs/heads/dev').toString().substr(0, 8);
}

let chiaPrice;
let lastData = {};

db.run("CREATE TABLE IF NOT EXISTS users (id INT, launcherID TEXT, wallet TEXT, lang TEXT)");

let getChiaPrice = () => {
    log('Getting current Chia price', 'debug');
    axios.get('https://xchscan.com/api/chia-price')
    .catch((err) => console.error(err.message))
    .then((response) => {
        if(response == undefined) return log('Failed to get Chia price', 'error');
        chiaPrice = response.data.usd.toFixed(2);
        if(!config.CHIA_PRICE_CHANNEL.enabled) return;
        client.channels.cache.get(config.CHIA_PRICE_CHANNEL.id).edit({ name: `Price: $${chiaPrice}/XCH`});
    });
}

getChiaPrice();
setInterval(getChiaPrice, m(5));

let netspaceChannel = () => {
    if(!config.NETSPACE_CHANNEL.enabled) return;
    log('Checking netspace', 'debug');
    channelUpdate.netspace(config.NETSPACE_API)
    .catch((err) => { return log(err, 'error')})
    .then((res) => {
        let netspace = res.data.netspace;
        if(lastData.netspace == null || lastData.netspace != netspace){
            lastData.netspace = netspace;
            client.channels.cache.get(config.NETSPACE_CHANNEL.id).edit({ name: `Netspace: ${convert.data(netspace)}`});
            return log(`Updated netspace to ${convert.data(netspace)}!`, 'info');
        }
    });
}
let poolsizeChannel = () => {
    if(!config.POOLSIZE_CHANNEL.enabled) return;
    log('Checking poolsize', 'debug');
    channelUpdate.netspace(config.POOLSIZE_API)
    .catch((err) => { return log(err, 'error')})
    .then((res) => {
        lastData.poolPoints = res.data.total_24hours_point;
        let poolspace = (res.data.total_24hours_point / 10) * 106278482545;
        if(lastData.poolsize == null || lastData.poolsize != poolspace){
            lastData.poolsize = poolspace;
            client.channels.cache.get(config.POOLSIZE_CHANNEL.id).edit({ name: `Poolsize: ${convert.data(poolspace)}`});
            return log(`Updated poolsize to ${convert.data(poolspace)}!`, 'info');
        }
    });
}
let poolBlocksToday = () => {
    if(!config.BLOCKS_TODAY_CHANNEL.enabled) return;
    log('Checking blocks today', 'debug');
    channelUpdate.blocks(config.BLOCKS_API)
    .catch((err) => { return log(err, 'error')})
    .then((res) => {
        let count = 0;
        for(let i=0;i<res.data.length;i++){
            let d = JSON.parse(res.data[i]);
            if(parseDate(d.time).isAfter(today())){
                count++;
            }
        }
        let blocksToday = `Blocks Today: ${count}`;
        if(blocksToday === lastData.blocksToday) return;
        lastData.blocksToday = blocksToday;
        log(`Updating blocks today to ${count}`, 'debug');
        return client.channels.cache.get(config.BLOCKS_TODAY_CHANNEL.id).edit({ name: blocksToday });
    });
}
let poolBlocksYday = () => {
    if(!config.BLOCKS_YDAY_CHANNEL.enabled) return;
    log('Checking blocks yesterday', 'debug');
    channelUpdate.blocks(config.BLOCKS_API)
    .catch((err) => { return log(err, 'error')})
    .then((res) => {
        let count = 0;
        for(let i=0;i<res.data.length;i++){
            let d = JSON.parse(res.data[i]);
            if(parseDate(d.time).isBetween(yesterday(), today())){
                count++;
            }
        }
        let blocksYday = `Blocks Yday: ${count}`;
        if(blocksYday === lastData.blocksYday) return;
        lastData.blocksYday = blocksYday;
        log(`Updating blocks yesterday to ${count}`, 'debug');
        return client.channels.cache.get(config.BLOCKS_YDAY_CHANNEL.id).edit({ name: blocksYday });
    });
}

let blocksPerDay = () => {
    if(!config.BLOCKS_PDAY_CHANNEL.enabled || lastData.poolsize == null || lastData.netspace == null) return;
    log('Checking blocks per day', 'debug');
    let blocksPerDay = Math.floor((lastData.poolsize / lastData.netspace) * 4608);
    if(lastData.blocksPerDay == null || lastData.blocksPerDay != blocksPerDay){
        lastData.blocksPerDay = blocksPerDay;
        log(`Updating blocks per day to ${blocksPerDay}`, 'debug');
        return client.channels.cache.get(config.BLOCKS_PDAY_CHANNEL.id).edit({ name: ` Est. Blocks: ${blocksPerDay}/day`});
    }
}

let luckLive = () => {
    if(!config.POOL_LUCK_CHANNEL.enabled || lastData.blocksPerDay == null) return;
    log('Checking current luck', 'debug');
    channelUpdate.blocks(config.BLOCKS_API)
    .catch((err) => { return log(err, 'error')})
    .then((res) => {
        if(res == null) return;
        let ETW = (24 / lastData.blocksPerDay) * 60;
        let lastBlock = JSON.parse(res.data[0]).time * 1000;
        let now = new Date();
        let diff = now - lastBlock;
        let mins = ((diff / 1000) / 60);
        log(`Updating luck to ${Math.round((mins / ETW) * 100)}%`, 'info');
        return client.channels.cache.get(config.POOL_LUCK_CHANNEL.id).edit({ name: `Luck Now: ${Math.round((mins / ETW) * 100)}%`});
    });
}

let luck24 = () => {
    if(lastData.blocksPerDay == null) return;
    log('Checking 24hour luck', 'debug');
    channelUpdate.blocks(config.BLOCKS_API)
    .catch((err) => { return log(err, 'error')})
    .then((res) => {
        let count = 0;
        for(let i=0;i<res.data.length;i++){
            let d = JSON.parse(res.data[i]);
            if(parseDate(d.time).isAfter(last24())) count++;
        }
        log(`Set 24hr luck to ${(lastData.blocksPerDay / count) * 100} %`, 'info');
        return lastData.luck24 = (lastData.blocksPerDay / count) * 100
    });
}

let luck7d = () => {
    if(lastData.blocksPerDay == null) return;
    log('Checking 7-day luck', 'debug');
    channelUpdate.blocks(config.BLOCKS_API)
    .catch((err) => { return log(err, 'error')})
    .then((res) => {
        let count = 0;
        for(let i=0;i<res.data.length;i++){
            let d = JSON.parse(res.data[i]);
            if(parseDate(d.time).isAfter(last7())) count++;
        }
        log(`Set 7-day luck to ${(lastData.blocksPerDay * 7 / count) * 100} %`, 'info');
        return lastData.luck7d = ((lastData.blocksPerDay * 7) / count) * 100;
    });
}

let luckHistory = () => {
    if(lastData.luck24 == null || lastData.luck7d == null) return;
    log(`Setting luck history to Luck 24h: ${Math.round(lastData.luck24)}% 7d: ${Math.round(lastData.luck7d)}%`, 'info');
    return client.channels.cache.get(config.POOL_LUCK_HISTORY_CHANNEL.id).edit({name: `Luck 24h: ${Math.round(lastData.luck24)}% 7d: ${Math.round(lastData.luck7d)}%`});
}

let blockWin = () => {
    log('Checking for block wins', 'debug');
    if(lastData.blockWin == null) lastData.blockWin = Date.now();
    channelUpdate.blocks(config.BLOCKS_API)
    .catch((err) => { return log(err, 'error') })
    .then((res) => {
        let count = 0;
        let heights = [];
        for(let i=0;i<res.data.length;i++){
            let d = JSON.parse(res.data[i]);
            if(lastData.blockWin < (d.time * 1000)){
                count++;
                heights.push(d.height);
            }
        }
        if(count === 0) return;
        let lastTime = JSON.parse(res.data[0]).time * 1000;
        log(lastTime, 'debug');
        lastData.blockWin = lastTime;
        poolBlock(client, config.POOL_BLOCK_CHANNEL.id, count, lastTime, heights, config.POOL_WALLET, build)
        .then((message) => {
            message.crosspost();
        })
        .catch((err) => { return log(err, 'error')});
        log(`${count} Blocks won!`, 'debug');
    });
}

const stats = new SlashCommandBuilder()
    .setName('stats')
    .setDescription('Replies with your stats');

const register = new SlashCommandBuilder()
    .setName('register')
    .setDescription('Register your launcher ID with your Discord account')
    .addStringOption(option => 
        option.setName('launcherid')
        .setDescription('Your Launcher ID')
        .setRequired(true))
    .addStringOption(option =>
        option.setName('lang')
        .setDescription('Language for bot replies')
        .setRequired(true)
        .addChoice('English', 'en')
        .addChoice('Français', 'fr')
        .addChoice('Deutsch', 'de')
        .addChoice('Ελληνικά', 'el')
        .addChoice('Русский', 'ru'))
    .addStringOption(option => 
        option.setName('wallet')
        .setDescription('Your wallet address to see your balance on the stats command')
        .setRequired(false));

const help = new SlashCommandBuilder()
    .setName('help')
    .setDescription('Help with using the bot!')
    .addStringOption(option =>
        option.setName('lang')
        .setDescription('Optional language of reply')
        .setRequired(false)
        .addChoice('English', 'en')
        .addChoice('Français', 'fr')
        .addChoice('Deutsch', 'de')
        .addChoice('Ελληνικά', 'el')
        .addChoice('Русский', 'ru'));

const rawData = [];

rawData.push(stats.toJSON());
rawData.push(register.toJSON());
rawData.push(help.toJSON());

const rest = new REST({ version: '9' }).setToken(config.DISCORD_TOKEN);

(async () => {
  try {
    log('Started refreshing application (/) commands.', 'info');

    await rest.put(
      Routes.applicationCommands(config.DISCORD_CLIENTID, config.GUILD_ID),
      { body: rawData },
    );

    log('Successfully reloaded application (/) commands.', 'info');
  } catch (error) {
    console.error(error);
  }
})();
client.on('ready', () => {
  log(`Logged in as ${client.user.tag}!`, 'info');
  client.user.setPresence({ activities: [{ name: '/stats', type: 'LISTENING'}]});
  schedule(netspaceChannel, m(5));
  schedule(poolsizeChannel, m(5));
  schedule(luckLive, m(5));
  schedule(poolBlocksToday, m(1));
  schedule(poolBlocksYday, m(1));
  schedule(blockWin, m(1));
  schedule(blocksPerDay, m(1));
  schedule(luck24, m(1));
  schedule(luck7d, m(1));
  schedule(luckHistory, m(1));
});

client.on('rateLimit', (rld) => {
    console.log(rld);
})

client.on('interactionCreate', async interaction => {
  if (!interaction.isCommand()) return;

  if(interaction.commandName === 'register'){
    log(pm2.register(), 'debug');
    log(`Got ${interaction.commandName} from ${interaction.user.username}#${interaction.user.discriminator}!`, 'debug');
    let lang = interaction.options.getString('lang');
    await interaction.deferReply({ ephemeral: true });
    let lID = interaction.options.getString('launcherid');
    if(lID.length !== 64 && lID.length !== 66){
        return interaction.editReply(l10n[lang].invalid_launcherid);
    }
    if(lID.length === 66 && lID.substr(0, 2) === '0x') lID = lID.substr(2, 64);
    db.all("SELECT * FROM users WHERE id = ?", interaction.user.id, (err, row) => {
        if(err) return console.error(err);
        if(row.length === 0){
            if(interaction.options.getString('wallet') != null){
                let wallet = interaction.options.getString('wallet');
                if(/^[a-zA-Z0-9]{62,62}/g.test(wallet) && wallet.substr(0, 4) === 'xch1'){
                    db.run("INSERT INTO users VALUES (?,?,?,?)", interaction.user.id, lID, wallet, lang);
                    return interaction.editReply(`${l10n[lang].set_launcherid} \`${lID}\`\n${l10n[lang].set_wallet} \`${wallet}\`\n${l10n[lang].set_language} ${l10n[lang].lang}`);
                } else {
                    return interaction.editReply(`${l10n[lang].invalid_wallet} \`${wallet}\``);
                }
            }
            db.run("INSERT INTO users VALUES (?,?,?,?)", interaction.user.id, lID, null, lang);
            return interaction.editReply(`${l10n[lang].set_launcherid} \`${lID}\``);
        } else {
            if(row[0].launcherID === lID){
                if(interaction.options.getString('wallet') != null){
                    let wallet = interaction.options.getString('wallet');
                    if(/^[a-zA-Z0-9]{62,62}/g.test(wallet) && wallet.substr(0, 4) === 'xch1'){
                        db.run("UPDATE users SET wallet = ? WHERE id = ?", wallet, interaction.user.id);
                        return interaction.editReply(`${l10n[lang].set_wallet} \`${wallet}\``);
                    } else {
                        return interaction.editReply(`${l10n[lang].invalid_wallet} \`${wallet}\``);
                    }
                }
                if(lang !== row[0].lang){
                    db.run("UPDATE users SET lang = ? WHERE id = ?", lang, interaction.user.id);
                    return interaction.editReply(`${l10n[lang].set_language} ${l10n[lang].lang}`);
                }
                return interaction.editReply(`${l10n[lang].launcherid_already_set} \`${lID}\``);
            }
            db.run("UPDATE users SET launcherID = ? WHERE id = ?", lID, interaction.user.id);
            return interaction.editReply(`${l10n[lang].update_launcherid} \`${lID}\``);
        }
    });
  }

  if(interaction.commandName === 'stats') {
    log(pm2.stats(), 'debug');
    log(`Got ${interaction.commandName} from ${interaction.user.username}#${interaction.user.discriminator}!`, 'debug');
    await interaction.deferReply({ ephemeral: true });
    let userConfig = {
        "joined": true,
        "unpaid": true,
        "last": true,
        "wal": false,
        "space": true,
        "sbp": true,
        "xchpd": true,
        "dpd": true
    }
    db.all("SELECT * FROM users WHERE id = ?", interaction.user.id, (err, row) => {
        if(err) return console.error(err);
        if(row.length === 0){
            return interaction.editReply('You need to register first! Please use `/register <launcherID>`');
        } else {
            let lang = row[0].lang;
            const embed = {
                color: 0x2ecc71,
                title: l10n[lang].stats_title,
                url: 'https://pool.poolchia.com',
                author: {
                    name: 'PoolChia.com Stats Bot',
                    icon_url: 'https://cdn.discordapp.com/icons/839994877479223296/5c17ebde41d03dabccb250709e391cb8.png?size=128',
                    url: 'https://pool.poolchia.com',
                },
                timestamp: new Date(),
                footer: {
                    text: build
                }
            };
            if(row[0].wallet != null) userConfig.wal = true;
            const sendData = (embed) => {
                interaction.editReply({ embeds: [ embed ]})
                .catch((err) => {
                    interaction.editReply(l10n[lang].stats_error);
                    return log(err, 'error');
                });
            }
            axios.get(`https://pool.poolchia.com/api/farmers/${row[0].launcherID}`)
            .catch((err) => {
                return console.error(err);
            })
            .then((response) => {
                let farmer = response.data;
                axios.get(`https://pool.poolchia.com/api/payments/${row[0].launcherID}`)
                .catch((err) => console.error(err))
                .then((response) => {
                    let payment
                    if(response.data.length == 0) userConfig.last = false
                    else if(response.data[0].includes('fee')) payment = JSON.parse(response.data[0].split('fee')[0]);
                    else payment = JSON.parse(response.data[0]);
                    embed.description = `Launcher ID: ${row[0].launcherID}`;
                    embed.fields = [];
                    if(userConfig.joined) embed.fields.push({name: l10n[lang].joined, value: new Intl.DateTimeFormat(lang, { dateStyle: 'full', timeStyle: 'long', timeZone: 'UTC' }).format(farmer.join_date * 1000)});
                    if(userConfig.unpaid) embed.fields.push({name: l10n[lang].unpaid_balance, value: `${farmer.unpaid / 1e12} XCH (${((farmer.unpaid / 5e9) * 100).toFixed(1)}% ${l10n[lang].minimum_payment})`});
                    if(userConfig.last) embed.fields.push({name: l10n[lang].last_payment, value: `${payment.amount / 1e12} XCH on ${new Intl.DateTimeFormat(lang, { timeStyle: 'long', dateStyle: 'medium', timeZone: 'UTC'}).format(payment.time * 1000)} (${elapsed(payment.time * 1000, lang)})`});
                    else embed.fields.push({name: l10n[lang].last_payment, value: `None yet :cry:`});
                    if(userConfig.wal){
                        axios.get(`https://xchscan.com/api/account/balance?address=${row[0].wallet}`)
                        .catch((err) => { return log(err, 'error')})
                        .then((res) => {
                            if(res.data == undefined) return interaction.editReply(l10n[lang].stats_error);
                            if(userConfig.wal) embed.fields.push({name: l10n[lang].wallet_balance, value: `${res.data.xch} XCH ($${(res.data.xch * chiaPrice).toFixed(2)} *$${chiaPrice}/XCH*)`});
                            if(userConfig.space) embed.fields.push({name: l10n[lang].est_space, value: convert.data(farmer.estimation)});
                            if(userConfig.sbp) embed.fields.push({name: l10n[lang].est_share, value: `${l10n[lang]['24hr_points']}: ${farmer.current_24hours_points}\n${l10n[lang].pool_points}: ${lastData.poolPoints}`});
                            if(userConfig.xchpd) embed.fields.push({name: l10n[lang].est_xch, value: (((farmer.estimation / lastData.netspace)*4608)*2).toFixed(12)});
                            if(userConfig.dpd) embed.fields.push({name: l10n[lang].est_usd, value: `$${((((farmer.estimation / lastData.netspace)*4608)*2)* chiaPrice).toFixed(2)} (*$${chiaPrice}/XCH*)`});
                            embed.fields.push({name: l10n[lang].contributing, value: `:tools: [${l10n[lang].contrib_source}](https://gitlab.com/1Megu/stats-bot)\n:globe_with_meridians: [${l10n[lang].contrib_translate}](https://crowdin.com/project/poolchia-stats-bot)`});
                            sendData(embed);
                        });
                    } else {
                        if(userConfig.space) embed.fields.push({name: l10n[lang].est_space, value: convert.data(farmer.estimation)});
                        if(userConfig.sbp) embed.fields.push({name: l10n[lang].est_share, value: `${l10n[lang]['24hr_points']}: ${farmer.current_24hours_points}\n${l10n[lang].pool_points}: ${lastData.poolPoints}`});
                        if(userConfig.xchpd) embed.fields.push({name: l10n[lang].est_xch, value: (((farmer.estimation / lastData.netspace)*4608)*2).toFixed(12)});
                        if(userConfig.dpd) embed.fields.push({name: l10n[lang].est_usd, value: `$${((((farmer.estimation / lastData.netspace)*4608)*2)* chiaPrice).toFixed(2)} (*$${chiaPrice}/XCH*)`});
                        embed.fields.push({name: l10n[lang].contributing, value: `:tools: [${l10n[lang].contrib_source}](https://gitlab.com/1Megu/stats-bot)\n:globe_with_meridians: [${l10n[lang].contrib_translate}](https://crowdin.com/project/poolchia-stats-bot)`});
                        sendData(embed);
                    }
                });
            });
        }
    });
    }

    if(interaction.commandName === 'help'){
        await interaction.deferReply({ ephemeral: true });
        interaction.editReply('Coming soon:tm:!')
    }


});

setInterval(() => {
    log(pm2.updateUsers(db), 'debug');
}, m(2));

client.login(config.DISCORD_TOKEN);
